module.exports = function (RED) {
    var nodeads = require('node-ads-api');


    function parseADSReturnValue(dataName, result) {
        var value;
        switch (dataName) {
            case 'BOOL':
                value = parseInt(result) != 0
                break;
            case 'BYTE':
                value = result;
                break;
            case 'INT':
                value = parseInt(Buffer.from(result).toString("hex"), 16)
                break;
            case 'STRING':
                value = Buffer.from(result).toString();
                break;
        }
        return value
    }

    function version(node) {
        return RED.version();
    }


    function AdsReadNode(config) {

        RED.nodes.createNode(this, config);


        var node = this;

        version(node);
        node.on('input', function (msg, send, done) {
            node.status({fill: "orange", shape: "dot", text: "Reading..."});

            var options = {
                //The IP or hostname of the target machine
                host: config.host,
                //The NetId of the target machine
                // amsNetIdTarget: "5.54.245.250.1.1",
                amsNetIdTarget: config.amsNetIdTarget,
                //The NetId of the source machine.
                //You can choose anything in the form of x.x.x.x.x.x,
                //but on the target machine this must be added as a route.
                amsNetIdSource: config.amsNetIdSource,

                //OPTIONAL: (These are set by default)
                //The tcp destination port
                port: config.port,
                //The ams source port
                amsPortSource: config.amsPortSource,
                //The ams target port for TwinCat 2 Runtime 1
                //amsPortTarget: 801,
                amsPortTarget: config.amsPortTarget,
                //The ams target port for TwinCat 3 Runtime 1
                //amsPortTarget: 851
                //The timeout for PLC requests
                timeout: config.timeout,

            }

            let myHandle = {
                indexOffset: config.indexOffset,
                indexGroup: config.indexGroup,
                bytelength: parseInt(config.bytelength),
                propname: 'value'
            }

            let datatype = config.datatype;

            if (msg.config) {
                if (typeof msg.config.host !== 'undefined') {
                    options.host = msg.config.host
                }
                if (typeof msg.config.host !== 'undefined') {
                    options.host = msg.config.host
                }
                if (typeof msg.config.amsNetIdTarget !== 'undefined') {
                    options.amsNetIdTarget = msg.config.amsNetIdTarget
                }
                if (typeof msg.config.amsNetIdSource !== 'undefined') {
                    options.amsNetIdSource = msg.config.amsNetIdSource
                }
                if (typeof msg.config.port !== 'undefined') {
                    options.port = msg.config.port
                }
                if (typeof msg.config.amsPortSource !== 'undefined') {
                    options.amsPortSource = msg.config.amsPortSource
                }
                if (typeof msg.config.amsPortTarget !== 'undefined') {
                    options.amsPortTarget = msg.config.amsPortTarget
                }
                if (typeof msg.config.timeout !== 'undefined') {
                    options.timeout = msg.config.timeout
                }
                if (typeof msg.config.indexOffset !== 'undefined') {
                    myHandle.indexOffset = msg.config.indexOffset
                }
                if (typeof msg.config.indexGroup !== 'undefined') {
                    myHandle.indexGroup = msg.config.indexGroup
                }
                if (typeof msg.config.bytelength !== 'undefined') {
                    myHandle.bytelength = parseInt(msg.config.bytelength)
                }
                if (typeof msg.config.datatype !== 'undefined') {
                    datatype = msg.config.datatype
                }
            }


            let identifier = options.amsNetIdTarget;

            if (typeof this.context().global.get(identifier) != 'undefined' &&
                this.context().global.get(identifier) != undefined
                && this.context().global.get(identifier).amsPortTarget != options.amsPortTarget
            ) {
                node.warn("Force reconnect caused by different ports");
                this.context().global.get(identifier).adsclient.end();

            }

            if (typeof this.context().global.get(identifier) == 'undefined' || this.context().global.get(identifier) == undefined || this.context().global.get(identifier).amsPortTarget != options.amsPortTarget) {

                let adsclient = nodeads.connect(options, function () {
                    node.warn("Connected to " + options.amsNetIdTarget + ":" + options.amsPortTarget);
                })

                adsclient.on('error', function (error) {
                    node.error(error);
                    if (typeof node.context().global.get(identifier) != 'undefined' &&
                        node.context().global.get(identifier) != undefined) {

                        if (typeof node.context().global.get(identifier).end === "function") {
                            node.context().global.get(identifier).end();
                        }

                        node.context().global.set(identifier, undefined);
                    }

                    node.status({fill: "red", shape: "dot", text: "Connection-Error" + msg.payload.errno});
                })

                this.context().global.set(identifier, {
                    adsclient: adsclient,
                    amsPortTarget: options.amsPortTarget
                });
            }

            this.context().global.get(identifier).adsclient.read(myHandle, function (err, result) {

                    if (err) {
                        msg.payload = err
                        node.warn(err);
                        node.status({fill: "red", shape: "dot", text: "Error: " + msg.payload.toString()});

                    } else {
                        let statusValue = "Success";
                        if (datatype != "RAW") {
                            if (result && result.value != null) {
                                msg.payload = parseADSReturnValue(datatype, result.value)
                                statusValue = msg.payload;
                            } else {
                                msg.payload = "";
                            }
                        } else {
                            msg.payload = result;
                        }


                        node.status({fill: "green", shape: "dot", text: statusValue});
                    }
                    node.send(msg);

                    if (typeof done === "function") {
                        done();
                    }
                }
            )


        });


    }

    RED.nodes.registerType("ADS Read", AdsReadNode);
}
