As addition to the Node <i>ADS-IN</i>, this Node allows you to directly read with indexOffset and indexGroup
address.

This is especially useful if you connected a sensor (e.g. via IO-Link) to your PLC and want to read values
directly from the sensor.

In this case you really don't want to create a symbol within the PLC-Programm for performance reasons.